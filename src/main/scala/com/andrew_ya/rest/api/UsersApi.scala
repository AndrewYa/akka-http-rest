package com.andrew_ya.rest.api

import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import com.andrew_ya.rest.repo.Repository

import scala.concurrent.ExecutionContext

/**
  * 1. GET запрос: отправить проекцию proj4 строкой, ответ должен быть EPSG код проекции
  * (если он есть, иначе вернуть сообщение о том, что проекции нет, json)
  * 2. POST запрос: 4 параметра: x (double), y (double), proj4from (строка), proj4to (строка); координаты (x, y)
  * в проекции proj4from преобразовать в координаты (m, n) в проекции proj4to
  */
trait UsersApi extends Repository { this: Repository =>
  def projectionRoutes(implicit context: ExecutionContext, materializer: ActorMaterializer) =

      get {
        path("epsg" / ".+".r) { projDesc =>
          complete{
            HttpEntity(ContentTypes.`text/plain(UTF-8)`, "Req " + getEpsgByProj(projDesc))
          }
        }
      } ~ post {
        path("transform") {
          formFields('p, 'proj4to, 'x.as[Double], 'y.as[Double]) {
            (proj4from, proj4to, x, y) =>
              complete{
                HttpEntity(ContentTypes.`text/plain(UTF-8)`, "Params " +(proj4from, proj4to, x, y))
              }
          }
        }
      }

}
