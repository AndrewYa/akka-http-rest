package com.andrew_ya.rest

import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import com.andrew_ya.rest.api.UsersApi

import scala.concurrent.ExecutionContext


trait Routes extends UsersApi {

    def routes(implicit context: ExecutionContext, materializer: ActorMaterializer) = pathPrefix("v1") {
      projectionRoutes
    }

}
