package com.andrew_ya.rest.connection

import slick.driver.JdbcProfile

/**
  * Created by andrew on 05.04.16.
  */
trait DBConnector {
  val driver: JdbcProfile
  import driver.api._
  val db: Database
}
