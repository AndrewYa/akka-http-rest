package com.andrew_ya.rest.repo

import org.gdal.osr.SpatialReference

/**
  * Created by andrew on 05.04.16.
  */
trait Repository {


    def getEpsgByProj(proj: String): String = {
        val dst = new SpatialReference("")
        dst.ImportFromProj4("+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs")
        dst.AutoIdentifyEPSG().toString
    }


//    def encode(from: String, to: String, x: Double, y: Double): (Double, Double) = {
//
//    }

}
